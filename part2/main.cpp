/*
    This is program is designed by Shaoxi He 11965252, Nathan, Karl
    For the final CG assignment.
    It is supposed to display 3 different objects.
    Each of them will be animated to perform rotation, tranlation with reference of a given inital point
    Each of them will have texture mapping
*/

#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
#include <GL/glut.h>
#include <Windows.h>
#include "SOIL.h"

struct Vector{
    GLfloat x;
    GLfloat y;
    GLfloat z;

    Vector(GLfloat x, GLfloat y, GLfloat z){
        this->x = x;
        this->y = y;
        this->z = z;
    }
};

using namespace std;

/****** Window and project configurations ******/
GLdouble xmin1 = -800, xmax1 = 800, ymin1 = -800, ymax1 = 800, znear1 = -800, zfar1 = 800;
Vector *point1 = new Vector(52.6977f, 245.581f, 369.552f);
Vector *point2 = new Vector(147.302f, -45.5814f, -369.552f);
Vector *initialPosition = new Vector(-127.567f, 170.994f, 392.611f);

/****** Lighting ******/
// Light 1 remote light
GLfloat light1_position[4] = { -50.0, -50.0, -50.0, 0.0 };
GLfloat light1_ambient[] = { 0.8, 0.3, 0.2, 1.0 };
GLfloat light1_diffuse[] = { 1, 1, 1, 1.0 };
GLfloat light1_specular[] = { 1.0, 1.0, 1.0, 1.0 };

// Light 2 local spot light
GLfloat light2_position[4] = { 100.0, 290.0, 185.0, 1.0 };
GLfloat light2_ambient[] = { 0.8, 0.5, 0.2, 1.0 };
GLfloat light2_diffuse[] = { 1, 1, 1, 1.0 };
GLfloat light2_specular[] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat angularLimit5 = 50.0;
GLfloat attenuationExponent5 = 2.0;
GLfloat dirVect[] = { 0.0, -1.0, 0.0 };

/****** Global configurations ******/
GLenum displayMode = GL_LINE;
GLint sleepInterval = 50;
GLenum shadeModel = GL_FLAT;
GLint objectSelection = 0;
GLint textureSelection = 0;
GLint objectColourSelection = 0;
// 0 for static, 1 for rotation, 2 for translation, 3 for translation with scaling
GLint animationSelection = 1; 
GLfloat translationSteps = 100;
GLfloat currentTranslationStep = 0;
GLfloat portion = 0;
Vector *translationVector = new Vector(point2->x - point1->x, point2->y - point1->y, point2->z - point1->z);
Vector *differenceFromInitial = new Vector(point1->x - initialPosition->x, point1->y - initialPosition->y, point1->z - initialPosition->z);
GLfloat biggestScale = 1.5;
GLfloat scaleStep = 0.005;
Vector *scaleVector = new Vector(1, 1, 1);
bool goingLarge = true;
GLfloat yAngle = 0.0;
GLfloat yStep = 3.0;

/****** Shaoxi He's Object properties ******/
GLdouble hexPyramid[8][3];
GLfloat radius = 100;
GLint height = radius * 2;
GLuint hexPyramidTextureIds[2];
const GLint lineTextureWidth = 16;
GLubyte textureLine[lineTextureWidth];
char rockTextureFileName[] = "rock.png";

/****** Nathan Richardson's Object properties ******/
GLint faceIndex[4][3] = {3, 2, 0,
						3, 0, 1,
						3, 1, 2,
						2, 1, 0};

GLfloat tetraPt[4][3] = { -50 + initialPosition->x, -50 + initialPosition->y, -50 + initialPosition->z,
                        50 + initialPosition->x, 50 + initialPosition->y, -50 + initialPosition->z,
                        50 + initialPosition->x, -50 + initialPosition->y, 50 + initialPosition->z,
                        -50 + initialPosition->x, 50 + initialPosition->y, 50 + initialPosition->z };
GLuint pyramidTextureIds[3];
char prismTextureFileName[] = "prism.jpg";
GLubyte textureLine2[lineTextureWidth];

void calculateHexPyramid(GLfloat center[3]) {
	GLfloat temp = 2.0 * M_PI / 6.0;
	temp = temp/2;
	GLfloat centerX, centerY, centerZ;
	centerX = center[0];
	centerY = center[1];
	centerZ = center[2];

	hexPyramid[0][0] = centerX;
	hexPyramid[0][1] = centerY + height;
	hexPyramid[0][2] = centerZ;

	hexPyramid[1][0] = centerX;
	hexPyramid[1][1] = centerY;
	hexPyramid[1][2] = centerZ - radius;

	hexPyramid[2][0] = centerX + radius * cos(temp);
	hexPyramid[2][1] = centerY;
	hexPyramid[2][2] = centerZ - radius * sin(temp);

	hexPyramid[3][0] = centerX + radius * cos(temp);
	hexPyramid[3][1] = centerY;
	hexPyramid[3][2] = centerZ + radius * sin(temp);

	hexPyramid[4][0] = centerX;
	hexPyramid[4][1] = centerY;
	hexPyramid[4][2] = centerZ + radius;

	hexPyramid[5][0] = centerX - radius * cos(temp);
	hexPyramid[5][1] = centerY;
	hexPyramid[5][2] = centerZ + radius * sin(temp);

	hexPyramid[6][0] = centerX - radius * cos(temp);
	hexPyramid[6][1] = centerY;
	hexPyramid[6][2] = centerZ - radius * sin(temp);

	hexPyramid[7][0] = hexPyramid[1][0];
	hexPyramid[7][1] = hexPyramid[1][1];
	hexPyramid[7][2] = hexPyramid[1][2];
}

void setLightSource1Property()
{
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);

	glLightfv(GL_LIGHT1, GL_AMBIENT, light1_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light1_specular);

	glColor3f(1.0, 1.0, 1.0);
	glPointSize(10.0);
	glBegin(GL_POINTS);
	glVertex3f(light1_position[0], light1_position[1], light1_position[2]);
	glEnd();
	glPointSize(1.0);
	glFlush();
}

void setLightSource2Property()
{
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);

	glLightfv(GL_LIGHT2, GL_AMBIENT, light2_ambient);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, light2_diffuse);
	glLightfv(GL_LIGHT2, GL_SPECULAR, light2_specular);

	glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, dirVect); // 0.
	glLightf(GL_LIGHT2, GL_SPOT_CUTOFF, angularLimit5);
	glLightf(GL_LIGHT2, GL_SPOT_EXPONENT, attenuationExponent5);

	glColor3f(1.0, 1.0, 1.0);
	glPointSize(10.0);
	glBegin(GL_POINTS);
	glVertex3f(light2_position[0], light2_position[1], light2_position[2]);
	glEnd();
	glPointSize(1.0);
	glFlush();

}

void createLineTexture2(GLint textureId, GLubyte texLine[]){
    texLine[0] = 253;
    texLine[1] = 71;
    texLine[2] = 165;
    texLine[3] = 255;

    texLine[4] = 144;
    texLine[5] = 151;
    texLine[6] = 229;
    texLine[7] = 255;

    texLine[8] = 30;
    texLine[9] = 184;
    texLine[10] = 106;
    texLine[11] = 255;

    texLine[12] = 0;
    texLine[13] = 45;
    texLine[14] = 254;
    texLine[15] = 255;

    glBindTexture(GL_TEXTURE_1D, textureId);

    glTexImage1D(
        GL_TEXTURE_1D,
        0,
        GL_RGBA,
        lineTextureWidth / 4,
        0,
        GL_RGBA,
        GL_UNSIGNED_BYTE,
        texLine
    );
}

void createLineTexture(GLint textureId, GLubyte texLine[]){
    GLint k;
	for (k=0; k <= 2; k += 2){
		texLine[ 4 * k ] = 255;
		texLine[ 4 * k + 1] = 200;
		texLine[ 4 * k + 2] = 81;
		texLine[ 4 * k + 3] = 255;
	}

	for (k=1; k <= 3; k += 2){
		texLine[ 4 * k ] = 127;
		texLine[ 4 * k + 1] = 88;
		texLine[ 4 * k + 2] = 2;
		texLine[ 4 * k + 3] = 255;
	}

    glBindTexture(GL_TEXTURE_1D, textureId);

    glTexImage1D(
        GL_TEXTURE_1D,
        0,
        GL_RGBA,
        lineTextureWidth / 4,
        0,
        GL_RGBA,
        GL_UNSIGNED_BYTE,
        texLine
    );
}

void drawHexPyramidWithPlainColour(){
    glBegin(GL_TRIANGLE_FAN);
        switch(objectColourSelection){
            case 0:
                glColor3f(1.0, 0.5, 0.3); break;
            case 1:
                glColor3f(1.0, 0.85f, 0); break;
            case 2:
                glColor3f(0.5, 0.4, 0.2); break;
            default:
                glColor3f(1.0, 1.0, 1.0);           
                break;
        }

        for (int i = 0; i < 8; i++) {
            glVertex3dv(hexPyramid[i]);
        }
    glEnd();
    glFlush();
}
void drawHexPyramidWithImageTexture(){
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glBegin(GL_TRIANGLE_FAN);
		glColor3d(0.1, 0.4, 0.7);
		for (int i = 0; i < 8; i++) {
			switch (i){
				case 0:
					glTexCoord2d(0.0, 0.5); break;
				case 1:
				case 3:
				case 5:
				case 7:
					glTexCoord2d(0.0, 1.0); break;
				case 2:
				case 4:
				case 6:
					glTexCoord2d(1.0, 1.0); break;
				default: break;				
			}
			glVertex3dv(hexPyramid[i]);
		}
	glEnd();
	glFlush();
}

void loadRockTexture(GLint texId){
	texId = SOIL_load_OGL_texture(
		rockTextureFileName,
		SOIL_LOAD_AUTO,
		texId,
		SOIL_FLAG_DDS_LOAD_DIRECT
	);
}

void loadPrismTexture(GLint texId){
    texId = SOIL_load_OGL_texture(
		prismTextureFileName,
		SOIL_LOAD_AUTO,
		texId,
		SOIL_FLAG_DDS_LOAD_DIRECT
	);
}

void drawHexPyramidWithLineTexture(){
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);

	glBegin(GL_TRIANGLE_FAN);
		// glColor3f(255, 255, 255);
		for (int i = 0; i < 8; i++) {
			if (i == 1){
				glTexCoord1d(1.0);
			}else if (i == 0){
				glTexCoord1d(0.0);
			}
			glVertex3dv(hexPyramid[i]);
		}
	glEnd();
}

void drawPyramidWithPlainColour(){
    for (GLint i = 0; i < 4; i++){
        // trio(tetraPt, faceIndex[i]);
        glBegin(GL_POLYGON);
            switch(objectColourSelection){
                case 0:
                    glColor3f(0.3, 0.7, 0.95f); break;
                case 1:
                    glColor3f(0.33f, 0.56f, 0.5); break;
                case 2:
                    glColor3f(0.04f, 0.83f, 0.17f); break;
                default:
                    glColor3f(1.0, 1.0, 1.0);           
                    break;
            }
            for (GLint j = 0; j < 3; j++){
                glVertex3fv(tetraPt[faceIndex[i][j]]);
            }
        glEnd();
        glFlush();
    }
}

void drawPyramidWithImageTexture(){
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    for (GLint i = 0; i < 4; i++){
        // trio(tetraPt, faceIndex[i]);
        glBegin(GL_POLYGON);
            glColor3d(0.5, 0.2, 1.0);
            for (GLint j = 0; j < 3; j++){
                if (j == 1){
                    glTexCoord2d(1.0, 1.0);
                }else if (j == 0){
                    glTexCoord2d(0.5, 0.0);
                }else{
                    glTexCoord2d(0.0, 1.0);
                }

                glVertex3fv(tetraPt[faceIndex[i][j]]);
            }
        glEnd();
        glFlush();
    }
}

void drawPyramidWithBlendColour(){
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);

	for (GLint i = 0; i < 4; i++){
        // trio(tetraPt, faceIndex[i]);
        glBegin(GL_POLYGON);
            for (GLint j = 0; j < 3; j++){
                if (j == 1){
                    glTexCoord1d(1.0);
                }else if (j == 0){
                    glTexCoord1d(0.0);
                }else{
                    glTexCoord1d(0.0);
                }

                glVertex3fv(tetraPt[faceIndex[i][j]]);
            }
        glEnd();
        glFlush();
    }
}

void displayPyramid(){
    glLineWidth(5);
    glPointSize(5);

    switch(textureSelection){
        case 0:
            drawPyramidWithPlainColour();
            break;
        case 1:
            glBindTexture(GL_TEXTURE_1D, pyramidTextureIds[0]);
			glEnable(GL_TEXTURE_1D);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);
            drawPyramidWithBlendColour();
			glDisable(GL_TEXTURE_1D);
            break;
        case 2:
            glBindTexture(GL_TEXTURE_2D, pyramidTextureIds[1]);
			glEnable(GL_TEXTURE_2D);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
            drawPyramidWithImageTexture(); 
			glDisable(GL_TEXTURE_2D);
            break;
        case 3: 
            glBindTexture(GL_TEXTURE_2D, pyramidTextureIds[1]);
			glEnable(GL_TEXTURE_2D);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
            drawPyramidWithImageTexture(); 
			glDisable(GL_TEXTURE_2D);
            break;
        default: break;
    }

    glLineWidth(1);
    glPointSize(1);
}

void displayHexPyramid(){
    glLineWidth(5);
    glPointSize(5);

    // Display object with textures
    switch(textureSelection){
        case 0: // Plain colour
            drawHexPyramidWithPlainColour(); 
            break;
        case 1: // Blend colour
            glBindTexture(GL_TEXTURE_1D, hexPyramidTextureIds[0]);
			glEnable(GL_TEXTURE_1D);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);
			drawHexPyramidWithLineTexture();
			glDisable(GL_TEXTURE_1D);
            break;
        case 2: // Image
            glBindTexture(GL_TEXTURE_2D, hexPyramidTextureIds[1]);
			glEnable(GL_TEXTURE_2D);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
			drawHexPyramidWithImageTexture();
			glDisable(GL_TEXTURE_2D);
            break;
        case 3: // modulated
            glBindTexture(GL_TEXTURE_2D, hexPyramidTextureIds[1]);
			glEnable(GL_TEXTURE_2D);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
			drawHexPyramidWithImageTexture();
			glDisable(GL_TEXTURE_2D);
            break;
        default: break;
    }

     glLineWidth(1);
    glPointSize(1);
}

void drawAxis(GLdouble xmin, GLdouble xmax, GLdouble ymin, GLdouble ymax, GLdouble zfar)
{
	glBegin(GL_LINES);
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(xmin, 0.0, 0.0);
	glVertex3f(xmax, 0.0, 0.0);

	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(0.0, ymin, 0.0);
	glVertex3f(0.0, ymax, 0.0);

	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(0.0, 0.0, -zfar);
	glVertex3f(0.0, 0.0, zfar);

	glColor3f(1.0, 1.0, 1.0);
	glVertex3f(point1->x, point1->y, point1->z);
	glVertex3f(point2->x, point2->y, point2->z);
	glEnd();
}

void objectSelectionCallback(GLint option) {
    switch(option){
        case 0:
        case 1:
        case 2: objectSelection = option; break;
        default: cout << "Invalid object option" << endl;
    }
    glutPostRedisplay();
}

void displayModeSelectionCallback(GLint option){
    switch(option){
        case 0: displayMode = GL_LINE; break;
        case 1: displayMode = GL_FILL; break;
        case 2: displayMode = GL_POINT; break;
        default: cout << "Invalid display mode option" << endl;
    }
    glutPostRedisplay();
}

void textureSelectionCallback(GLint option){
    switch(option){
        case 0: // No texture
        case 1: // blend color texture
        case 2: // image with object color
        case 3: // image with modulated color 
            textureSelection= option; break;
        default: cout << "Invalid texture option" << endl;
    }
    glutPostRedisplay();
}

void objectColourSelectionCallback(GLint option){
    switch(option){
        case 0: //
        case 1: //
        case 2: //
            objectColourSelection = option; break;
        default: cout << "Invalid texture option" << endl;
    }
    glutPostRedisplay();
}

void idle(){
    switch (animationSelection){
        case 1: 
            break;
        case 2: 
            // rotating
            yAngle += yStep;
			if (yAngle > 360) {
				yAngle = 0.0;
			}
            break;
        case 3: 
            // translating
            currentTranslationStep += 1;
            currentTranslationStep += 1;
			if (currentTranslationStep > translationSteps){
				currentTranslationStep = 0;
			}
            break;
        case 4:
            currentTranslationStep += 1;

            if (goingLarge){
                scaleVector->x += scaleStep;
                scaleVector->y += scaleStep;
                scaleVector->z += scaleStep;
            }

            if (scaleVector->x > biggestScale){
                scaleVector->x = 1.0;
                scaleVector->y = 1;
                scaleVector->z = 1;
            }

            if (currentTranslationStep > translationSteps){
				currentTranslationStep = 0;
			}
            break;
    }

    glutPostRedisplay();
}

void init(){
    glClearColor(0.0, 0.0, 0.0, 0.0);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

	glOrtho(xmin1, xmax1, ymin1, ymax1, znear1, zfar1);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glRotatef(45, 1, 1, 0);

    glutIdleFunc(idle);

    // Initialisation for Hex pyramid
    GLfloat initialPosArray[3] = { initialPosition->x, initialPosition->y, initialPosition->z};
    calculateHexPyramid(initialPosArray);

    glGenTextures(2, &hexPyramidTextureIds[0]);
    createLineTexture(hexPyramidTextureIds[0], textureLine);
    loadRockTexture(hexPyramidTextureIds[1]);

    glGenTextures(2, &pyramidTextureIds[0]);
    createLineTexture2(pyramidTextureIds[0], textureLine2);
    loadPrismTexture(pyramidTextureIds[1]);
}

void displayCallback(){
    glClear(GL_COLOR_BUFFER_BIT);
	glClear(GL_DEPTH_BUFFER_BIT);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);

    setLightSource1Property();
	setLightSource2Property();

    glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);

	glShadeModel(shadeModel);
	drawAxis(xmin1, xmax1, ymin1, ymax1, zfar1);

    glPushMatrix();

    glPolygonMode(GL_FRONT_AND_BACK, displayMode);

    // Perform animation transformations
    switch(animationSelection){
        case 1: break;
        case 2: 
            glRotatef(yAngle, 0.0, 1.0, 0.0);
            break;
        case 3:
            glTranslatef(differenceFromInitial->x, differenceFromInitial->y, differenceFromInitial->z);
            portion = currentTranslationStep/translationSteps;
            glTranslatef(translationVector->x * portion, translationVector->y * portion, translationVector->z * portion);
            break;
        case 4:
            glTranslatef(differenceFromInitial->x, differenceFromInitial->y, differenceFromInitial->z);
            portion = currentTranslationStep/translationSteps;
            glTranslatef(translationVector->x * portion, translationVector->y * portion, translationVector->z * portion);

            glScalef(scaleVector->x, scaleVector->y, scaleVector->z);
            break;
    }

    /*
        Displaying objects here
    */
    switch (objectSelection){
        case 0:
            // Display Shaoxi's Object
            displayHexPyramid();
            break;
        case 1:
            // Display Nathan's Object
            displayPyramid();
            break;
        case 2:
            // Display Karl's Object
            break;
        default: cout << "Invalid object selection to display." << endl;
    }


    glPopMatrix();
	glDisable(GL_CULL_FACE);
	glDisable(GL_LIGHT1);
	glDisable(GL_LIGHT2);
	glDisable(GL_DEPTH_TEST);
	Sleep(sleepInterval);
	glutSwapBuffers();
	glFlush();
}

void keyFuncCallback(GLubyte key, GLint xMouse, GLint yMouse){
    GLfloat viewAngleStep = 5;

    GLint states = glutGetModifiers();

    switch (key){
        case '1': animationSelection = 1;break;
        case '2': 
            animationSelection = 2;
            cout << "Not implemented" << endl;
            break;
        case '3': animationSelection = 3;break;
        case '4': animationSelection = 4;break;
        case 'l': glEnable(GL_LIGHTING); break;
		case 'n': glDisable(GL_LIGHTING); break;
        case 'u':
		case 'U':
			sleepInterval-=5; 
			if (sleepInterval < 0){
				sleepInterval = 0;
			} 
			break;
		case 'd':
		case 'D': sleepInterval+=5; break;
        case 'f': // Constant intensity rendering
		case 'F': glShadeModel(GL_FLAT); break;
		case 's': // Gourand rendering
		case 'S': glShadeModel(GL_SMOOTH); break;
        case 'i': 
			init();
			glutPostRedisplay();
			break;
        case 'x':
        case 'X':
            if (states & GLUT_ACTIVE_SHIFT) glRotatef(-viewAngleStep, 1.0, 0.0, 0.0);
            else glRotatef(viewAngleStep, 1.0, 0.0, 0.0);
			glutPostRedisplay();
            break;
        case 'y':
        case 'Y':
            if (states & GLUT_ACTIVE_SHIFT) glRotatef(-viewAngleStep, 0.0, 1.0, 0.0);
            else glRotatef(viewAngleStep, 0.0, 1.0, 0.0);
			glutPostRedisplay();
            break;
        case 'z':
        case 'Z':
            if (states & GLUT_ACTIVE_SHIFT) glRotatef(-viewAngleStep, 0.0, 0.0, 1.0);
			else glRotatef(viewAngleStep, 0.0, 0.0, 1.0);
            glutPostRedisplay();
            break;
        default: break;
    }
}

void attachMenus(){
    /*
        LEFT BUTTON MENUS
    */
    GLint displayModeSubMenu = glutCreateMenu(displayModeSelectionCallback);
    glutAddMenuEntry("Wireframe", 0);
    glutAddMenuEntry("Fill", 1);
    glutAddMenuEntry("Points", 2);

    glutCreateMenu(objectSelectionCallback);
    glutAddMenuEntry("Display Shaoxi's object: Hex-pyramid", 0);
    glutAddMenuEntry("Display Nathan's object: pyramid", 1);
    // glutAddMenuEntry("Display Karl's object: Unknown(Not implemented)", 2);
    glutAddSubMenu("Display mode", displayModeSubMenu);

    glutAttachMenu(GLUT_LEFT_BUTTON);

    /*
        Right BUTTON MENUS
    */
    GLint objectColourSubMenu = glutCreateMenu(objectColourSelectionCallback);
    glutAddMenuEntry("First colour", 0);
    glutAddMenuEntry("Second colour", 1);
    glutAddMenuEntry("Third colour", 2);

    glutCreateMenu(textureSelectionCallback);
    glutAddMenuEntry("Plain colour", 0);
    glutAddMenuEntry("Blend colour", 1);
    glutAddMenuEntry("Image texture", 2);
    glutAddMenuEntry("Modulated image texture", 3);
    glutAddSubMenu("Choose colour", objectColourSubMenu);

    glutAttachMenu(GLUT_RIGHT_BUTTON);
}

int main(int argc, char** argv){

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

    glutInitWindowPosition(50, 50);
    glutInitWindowSize(800, 800);

    glutCreateWindow("Group 1 He-Richardson Final Part 2");

    init();
    attachMenus();

    glutDisplayFunc(displayCallback);
    glutKeyboardFunc(keyFuncCallback);

    glutMainLoop();

    return 0;
}