/*
This program displays one animated cube with face and vertex normal vectors included
rotate  around the x-axis, translate along the y-axis
designed by Helen Lu
*/

#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>
#include <GL/glut.h>
#include <Windows.h>
#include "SOIL.h"

using namespace std;

struct Point{
	GLfloat x;
	GLfloat y;
	GLfloat z;
};

GLint windowWidth = 800, windowHeight = 800;   //  Initial display-window size.
GLdouble xmin1 = -800, xmax1 = 800, ymin1 = -800, ymax1 = 800, znear1 = -800, zfar1 = 800;
GLdouble xStep = 5, yStep = 5, zStep = 5;
GLdouble tx = 0, ty = 0, tz = 0;
GLdouble txStep = 5, tyStep = 5, tzStep = 5;
GLdouble xAngle = 0, yAngle = 0, zAngle = 0;
GLfloat rStep = 5.00000;
GLfloat point1xAngle;
GLfloat point1yAngle;
GLfloat point1zAngle;
GLfloat rotateAngle = 0.0;
GLint option = 1;
GLfloat red = 0.8, green = 0.8, blue = 0.8;

GLenum displayMode = GL_LINE;
GLint sleepInterval = 50;

GLfloat point1[3] = { 52.6977, 245.581, 369.552 };
GLfloat point2[3] = { 147.302, -45.5814, -369.552 };
GLfloat difference[3] = { point2[0] - point1[0], point2[1] - point1[1], point2[2] - point1[2] };
GLfloat translationSteps = 100;
GLfloat currentStep = 0;
GLfloat portion;
GLfloat biggest = 1.5;
GLfloat smallest = 0.8;
GLfloat scaleX = 1;
GLfloat scaleY = 1;
GLfloat scaleZ = 1;
bool goingLarge = true;

GLdouble hexPyramid[8][3];
GLfloat radius = 100;
GLint height = radius * 2;
GLfloat pyramidInitialPosition[3] = { -127.567, 170.994, 392.611 };
GLfloat differenceFromInitial[3] = { point1[0] - pyramidInitialPosition[0], point1[1] - pyramidInitialPosition[1], point1[2] -pyramidInitialPosition[2] };
// GLfloat pyramidInitialPosition[3] = { 0, 0, 0 };

// Light 2 remote light
GLfloat light2_position[4] = { -50.0, -50.0, -50.0, 0.0 };
GLfloat light2_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
GLfloat light2_diffuse[] = { 1, 1, 1, 1.0 };
GLfloat light2_specular[] = { 1.0, 1.0, 1.0, 1.0 };

// Light 3 local spot light
GLfloat light3_position[4] = { -180.0, 0.0, 0.0, 1.0 };
GLfloat light3_ambient[] = { 0.5, 0.5, 0.5, 1.0 };
GLfloat light3_diffuse[] = { 1, 1, 1, 1.0 };
GLfloat light3_specular[] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat angularLimit5 = 50.0;
GLfloat attenuationExponent5 = 2.0;
GLfloat dirVect[] = { 1.0, 0.0, 0.0 };

GLdouble side = 100;

// Texture mapping
static GLuint lineTextureId;
static GLuint imageTextureId;
char imageTextureFile[] = "rock.png";
const GLint imageWidth = 64;
const GLint imageHeight = 64;
GLubyte texArray[imageHeight][imageWidth][4];
// GLint height;
const GLint lineTexWidth = 16;
GLubyte texLine[lineTexWidth];
GLint textureOption = 1;

void init()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(xmin1, xmax1, ymin1, ymax1, znear1, zfar1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(45, 1, 1, 0);

}

void createLineTexturePattern(GLubyte lineTex[]){
	GLint k;
	for (k=0; k <= 2; k += 2){
		lineTex[ 4 * k ] = 255;
		lineTex[ 4 * k + 1] = 200;
		lineTex[ 4 * k + 2] = 81;
		lineTex[ 4 * k + 3] = 255;
	}

	for (k=1; k <= 3; k += 2){
		lineTex[ 4 * k ] = 127;
		lineTex[ 4 * k + 1] = 88;
		lineTex[ 4 * k + 2] = 2;
		lineTex[ 4 * k + 3] = 255;
	}
}

void create1dTexture(GLint texId, GLubyte lineTex[]){
	glBindTexture(GL_TEXTURE_1D, texId);

	glTexImage1D(
		GL_TEXTURE_1D,
		0,
		GL_RGBA,
		lineTexWidth / 4,
		0,
		GL_RGBA,
		GL_UNSIGNED_BYTE,
		texLine
	);
}

void loadImageTexture(GLint texId){
	texId = SOIL_load_OGL_texture(
		imageTextureFile,
		SOIL_LOAD_AUTO,
		texId,
		SOIL_FLAG_DDS_LOAD_DIRECT
	);
}

void setLightSource2Property()
{
	glLightfv(GL_LIGHT2, GL_POSITION, light2_position);

	glLightfv(GL_LIGHT2, GL_AMBIENT, light2_ambient);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, light2_diffuse);
	glLightfv(GL_LIGHT2, GL_SPECULAR, light2_specular);

	glColor3f(1.0, 1.0, 1.0);
	glPointSize(10.0);
	glBegin(GL_POINTS);
	glVertex3f(light2_position[0], light2_position[1], light2_position[2]);
	glEnd();
	glPointSize(1.0);
	glFlush();
}

void setLightSource3Property()
{
	glLightfv(GL_LIGHT3, GL_POSITION, light3_position);

	glLightfv(GL_LIGHT3, GL_AMBIENT, light3_ambient);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, light3_diffuse);
	glLightfv(GL_LIGHT3, GL_SPECULAR, light3_specular);

	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, dirVect); // 0.
	glLightf(GL_LIGHT3, GL_SPOT_CUTOFF, angularLimit5);
	glLightf(GL_LIGHT3, GL_SPOT_EXPONENT, attenuationExponent5);

	glColor3f(1.0, 1.0, 1.0);
	glPointSize(10.0);
	glBegin(GL_POINTS);
	glVertex3f(light3_position[0], light3_position[1], light3_position[2]);
	glEnd();
	glPointSize(1.0);
	glFlush();

}

void calculateHexPyramid(GLfloat center[3]) {
	GLfloat temp = 2.0 * M_PI / 6.0;
	temp = temp/2;
	GLfloat centerX, centerY, centerZ;
	centerX = center[0];
	centerY = center[1];
	centerZ = center[2];

	hexPyramid[0][0] = centerX;
	hexPyramid[0][1] = centerY + height;
	hexPyramid[0][2] = centerZ;

	hexPyramid[1][0] = centerX;
	hexPyramid[1][1] = centerY;
	hexPyramid[1][2] = centerZ - radius;

	hexPyramid[2][0] = centerX + radius * cos(temp);
	hexPyramid[2][1] = centerY;
	hexPyramid[2][2] = centerZ - radius * sin(temp);

	hexPyramid[3][0] = centerX + radius * cos(temp);
	hexPyramid[3][1] = centerY;
	hexPyramid[3][2] = centerZ + radius * sin(temp);

	hexPyramid[4][0] = centerX;
	hexPyramid[4][1] = centerY;
	hexPyramid[4][2] = centerZ + radius;

	hexPyramid[5][0] = centerX - radius * cos(temp);
	hexPyramid[5][1] = centerY;
	hexPyramid[5][2] = centerZ + radius * sin(temp);

	hexPyramid[6][0] = centerX - radius * cos(temp);
	hexPyramid[6][1] = centerY;
	hexPyramid[6][2] = centerZ - radius * sin(temp);

	hexPyramid[7][0] = hexPyramid[1][0];
	hexPyramid[7][1] = hexPyramid[1][1];
	hexPyramid[7][2] = hexPyramid[1][2];
}

void drawAxis(GLdouble xmin, GLdouble xmax, GLdouble ymin, GLdouble ymax, GLdouble zfar)
{
	glBegin(GL_LINES);
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(xmin, 0.0, 0.0);
	glVertex3f(xmax, 0.0, 0.0);

	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(0.0, ymin, 0.0);
	glVertex3f(0.0, ymax, 0.0);

	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(0.0, 0.0, -zfar);
	glVertex3f(0.0, 0.0, zfar);

	glColor3f(1.0, 1.0, 1.0);
	glVertex3f(point1[0], point1[1], point1[2]);
	glVertex3f(point2[0], point2[1], point2[2]);
	glEnd();

}

void drawHexPyramidWithLineTexture() {
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);

	glPolygonMode(GL_FRONT_AND_BACK, displayMode);

	glBegin(GL_TRIANGLE_FAN);
		// glColor3f(255, 255, 255);
		for (int i = 0; i < 8; i++) {
			if (i == 1){
				glTexCoord1d(1.0);
			}else if (i == 0){
				glTexCoord1d(0.0);
			}
			glVertex3dv(hexPyramid[i]);
		}
	glEnd();
}

void drawHexPyramidWithImageTexture(){
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glPolygonMode(GL_FRONT_AND_BACK, displayMode);

	glBegin(GL_TRIANGLE_FAN);
		glColor3d(0.1, 0.4, 0.7);
		for (int i = 0; i < 8; i++) {
			switch (i){
				case 0:
					glTexCoord2d(0.0, 0.5); break;
				case 1:
				case 3:
				case 5:
				case 7:
					glTexCoord2d(0.0, 1.0); break;
				case 2:
				case 4:
				case 6:
					glTexCoord2d(1.0, 1.0); break;
				default: break;				
			}
			glVertex3dv(hexPyramid[i]);
		}
	glEnd();
	glFlush();
}

void displayPyramidCallback()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glClear(GL_DEPTH_BUFFER_BIT);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);

	glShadeModel(GL_FLAT);
	drawAxis(xmin1, xmax1, ymin1, ymax1, zfar1);

	setLightSource2Property();
	setLightSource3Property();

	glEnable(GL_LIGHT2);
	glEnable(GL_LIGHT3);

	glPushMatrix();

	
	glPointSize(5);
	glLineWidth(5);

	switch (option) {
		case 1:
			glRotated(xAngle, 1, 0, 0);
			break;
		case 2:
			glRotated(yAngle, 0, 1, 0);
			break;
		case 3:
			glRotated(zAngle, 0, 0, 1);
			break;
		case 4:
			
			break;
		case 5:
			// Translate to point 1
			glTranslatef(differenceFromInitial[0], differenceFromInitial[1], differenceFromInitial[2]);

			// Translate by steps to point 2
			portion = currentStep/translationSteps;
			glTranslatef(difference[0] * portion, difference[1] * portion, difference[2] * portion);
			break;
		case 6:

			// Translate to point 1
			glTranslatef(differenceFromInitial[0], differenceFromInitial[1], differenceFromInitial[2]);

			// Translate by steps to point 2
			portion = currentStep/translationSteps;
			glTranslatef(difference[0] * portion, difference[1] * portion, difference[2] * portion);

			glScalef(scaleX, scaleY, scaleZ);
			break;
		case 7:
			break;
		default: cout << "Invalid display option" << endl; break;
	}

	switch(textureOption){
		case 1:
			glBindTexture(GL_TEXTURE_1D, lineTextureId);
			glEnable(GL_TEXTURE_1D);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);
			drawHexPyramidWithLineTexture();
			glDisable(GL_TEXTURE_1D);
			break;
		case 2:
			glBindTexture(GL_TEXTURE_2D, imageTextureId);
			glEnable(GL_TEXTURE_2D);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
			drawHexPyramidWithImageTexture();
			glDisable(GL_TEXTURE_2D);
			break;
		case 3:
			glBindTexture(GL_TEXTURE_2D, imageTextureId);
			glEnable(GL_TEXTURE_2D);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
			drawHexPyramidWithImageTexture();
			glDisable(GL_TEXTURE_2D);
	}


	glLineWidth(1);
	glPointSize(1);

	glPopMatrix();
	glDisable(GL_CULL_FACE);
	glDisable(GL_LIGHT2);
	glDisable(GL_LIGHT3);
	glDisable(GL_DEPTH_TEST);
	Sleep(sleepInterval);
	glutSwapBuffers();
	glFlush();
}

void idle() {
	switch (option) {
		case 1:
			xAngle += xStep;
			if (xAngle > 360) {
				xAngle = 0.0;
			}
			break;
		case 2:
			yAngle += yStep;
			if (yAngle > 360) {
				yAngle = 0.0;
			}
			break;
		case 3:
			zAngle += zStep;
			if (zAngle > 360) {
				zAngle = 0.0;
			}
			break;
		case 4:
			rotateAngle += rStep;
			if (rotateAngle > 360) {
				rotateAngle = 0.0;
			}
			break; 
		case 5:
		case 6:
			if (goingLarge){
				scaleX += 0.005;
				scaleY += 0.005;
				scaleZ += 0.005;
			}

			if (scaleX > biggest) {
				scaleX = 1;
				scaleY = 1;
				scaleZ = 1;
			}

			currentStep += 1;
			if (currentStep > translationSteps){
				currentStep = 0;
			}
			break;
		case 7:
			break;
		default:
			cout << "Invalid option." << endl;
			break;
	}

	glutPostRedisplay();
}

void keyFunc(GLubyte key, GLint xMouse, GLint yMouse)
{
	GLfloat viewAngleStep = 5;

	switch (key)
	{
		case 'l': glEnable(GL_LIGHTING); break;
		case 'k': glDisable(GL_LIGHTING); break;
		case 'q': glutIdleFunc(idle); break;
		case 'w': glutIdleFunc(NULL); break;
		case 'u':
		case 'U':
			sleepInterval-=5; 
			if (sleepInterval < 0){
				sleepInterval = 0;
			} 
			break;
		case 'd':
		case 'D': sleepInterval+=5; break;
		case 'f': // Constant intensity rendering
		case 'F': glShadeModel(GL_FLAT); break;
		case 's': // Gourand rendering
		case 'S': glShadeModel(GL_SMOOTH); break;
		case 'i': 
			init();
			glutPostRedisplay();
			break; 
		case 'z':
		case 'Z':
			glRotatef(viewAngleStep, 1.0, 0.0, 0.0);
			glutPostRedisplay();
			break;
		case 'x':
		case 'X':
			glRotatef(viewAngleStep, 0.0, 1.0, 0.0);
			glutPostRedisplay();
			break;
		case 'c':
		case 'C':
			glRotatef(viewAngleStep, 0.0, 0.0, 1.0);
			glutPostRedisplay();
			break;
		case 'v':
		case 'V':
			glRotatef(-viewAngleStep, 1.0, 0.0, 0.0);
			glutPostRedisplay();
			break;
		case 'b':
		case 'B':
			glRotatef(-viewAngleStep, 0.0, 1.0, 0.0);
			glutPostRedisplay();
			break;
		case 'n':
		case 'N':
			glRotatef(-viewAngleStep, 0.0, 0.0, 1.0);
			glutPostRedisplay();
			break;
		default:
			break;
	}
	glutPostRedisplay();

}

void reshapeFcn(GLint newWidth, GLint newHeight)
{
	glViewport(0, 0, newWidth, newHeight);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(xmin1, xmax1, ymin1, ymax1, znear1, zfar1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(45, 1, 1, 0);

}

void rotateSelection(GLint rotateSelection) {
	switch (rotateSelection) {
	case 1:
	case 2:
	case 3:
	case 4: cout << "Not implemented." << endl;
	// case 5:
	// 	exit(0);
	// 	break;
	case 5:
	case 6:
	case 7:
		option = rotateSelection;
		break;
	default: cout << "Invalid option" << endl; 
		break;
	}

	glutPostRedisplay();
}

void displaySelection(GLint displayOption){
	switch (displayOption){
		case 1: 
			displayMode = GL_LINE;
			break;
		case 2:
			displayMode = GL_FILL;
			break;
		case 3:
			displayMode = GL_POINT;
			break;
		default: 
			cout << "Invalid display mode." << endl;
			break;
	}

	glutPostRedisplay();
}

void textureSelection(GLint textureSelection){
	switch(textureSelection){
		case 1: 
		case 2:
		case 3:
			textureOption = textureSelection;
			break;
		default: cout << "Invalide texture option." << endl;
	}
	glutPostRedisplay();
}

int main(int argc, char** argv)
{
	GLint option = 1;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);

	glutInitWindowPosition(50, 50);
	glutInitWindowSize(windowWidth, windowHeight);

	glutCreateWindow("Group 1 Assignment");

	init();

	calculateHexPyramid(pyramidInitialPosition);

	createLineTexturePattern(texLine);
	glGenTextures(1, &lineTextureId);
	create1dTexture(lineTextureId, texLine);

	glGenTextures(1, &imageTextureId);
	loadImageTexture(imageTextureId);
	GLint textureMenu = glutCreateMenu(textureSelection);
	glutAddMenuEntry("Blend colours", 1);
	glutAddMenuEntry("Image with texture colour", 2);
	glutAddMenuEntry("Image with modulation", 3);

	GLint subMenu = glutCreateMenu(displaySelection);
	glutAddMenuEntry("Wireframe", 1);
	glutAddMenuEntry("Fill", 2);
	glutAddMenuEntry("Vertices", 3);


	glutCreateMenu(rotateSelection);
	glutAddSubMenu("Display", subMenu);
	glutAddSubMenu("Texture", textureMenu);
	glutAddMenuEntry("Rotate around x", 1);
	glutAddMenuEntry("Rotate around y", 2);
	glutAddMenuEntry("Rotate around z", 3);
	glutAddMenuEntry("Rotate around p1p2", 4);
	glutAddMenuEntry("Translate from p1 to p2", 5);
	glutAddMenuEntry("Translate from p1 to p2 with scaling", 6);
	
	glutAttachMenu(GLUT_LEFT_BUTTON);

	glutDisplayFunc(displayPyramidCallback);
	glutKeyboardFunc(keyFunc);
	glutReshapeFunc(reshapeFcn);


	glutMainLoop();

	return 0;
}