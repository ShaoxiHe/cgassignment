#include <Windows.h>
#include <math.h>
#include <glut.h>

GLint windowWidth = 600, windowHeight = 600;
GLdouble xmin1 = -500, xmax1 = 500, ymin1 = -500, ymax1 = 500, znear1 = -500, zfar1 = 500;

GLdouble ref1[3] = { -53.0734, 100, -369.552};
GLdouble ref2[3] = { 253.073, 100, 369.552 };
GLdouble grad = (ref2[2] - ref1[2]) / (ref2[0] - ref1[0]);

GLdouble origin[3] = { 357.083, 147.909, -0.0000000000000437 };

GLint faceIndex[4][3] = {3, 2, 0,
						3, 0, 1,
						3, 1, 2,
						2, 1, 0};

//GLfloat tetraPt[4][3] = { 0, 0, 0,
//						100, 100, 0,
//						100, 0, 100,
//						0, 100, 100 };

GLfloat tetraPt[4][3] = { -50, -50, -50,
						50, 50, -50,
						50, -50, 50,
						-50, 50, 50 };

GLint animOption = 0;
GLdouble xAngle = 0, yAngle = 0, zAngle = 0;
GLdouble xStep = 5, yStep = 5, zStep = 5;
GLdouble xPos = origin[0], yPos = origin[1], zPos = origin[2];
GLdouble transStep = 2;
GLdouble scaleFactor = 1.01;
GLdouble scaleCur = 1.0;

int refreshMills = 300;

void init()
{
	glClearColor(0, 0, 0, 0);


	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(xmin1, xmax1, ymin1, ymax1, znear1, zfar1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(45, 1, 1, 0);

}

void drawAxis(GLdouble xmin, GLdouble xmax, GLdouble ymin, GLdouble ymax, GLdouble zfar)
{
	glLineWidth(3);
	glBegin(GL_LINES);
	glColor3f(1.0, 0.0, 0.0);
	glVertex3f(xmin, 0.0, 0.0);
	glVertex3f(xmax, 0.0, 0.0);

	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(0.0, ymin, 0.0);
	glVertex3f(0.0, ymax, 0.0);

	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(0.0, 0.0, -zfar);
	glVertex3f(0.0, 0.0, zfar);

	glColor3f(1.0, 0.0, 1.0);
	glVertex3f(ref1[0], ref1[1], ref1[2]);
	glVertex3f(ref2[0], ref2[1], ref2[2]);

	glEnd();
	glLineWidth(1);
}

void trio(GLfloat tetraCoords[4][3], GLint face[3])
{
	glBegin(GL_LINES);
	for (GLint i = 0; i < 3; i++)
	{
		glColor3f(1, 1, 1);
		glVertex3fv(tetraCoords[face[i]]);

		if(i < 2)
			glVertex3fv(tetraCoords[face[i+1]]);
	}

	glEnd();
	glFlush();
}

void drawTetra(GLfloat tetraCoords[4][3])
{
	glLineWidth(5);
	for (GLint i = 0; i < 4; i++)
	{
		trio(tetraCoords, faceIndex[i]);
	}
	glLineWidth(1);
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glClear(GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	glPolygonMode(GL_FRONT_AND_BACK, GLU_LINE);

	drawAxis(xmin1, xmax1, ymin1, ymax1, zfar1);

	glPushMatrix();
	if (animOption == 1)
	{
		glRotated(xAngle, 1, 0, 0);
	}
	else if (animOption == 2)
		glTranslated(xPos, yPos, zPos);
	else if (animOption == 3)
	{
		glTranslated(xPos, yPos, zPos);
		glScaled(scaleCur, scaleCur, scaleCur);
	}
	else
		glTranslated(origin[0], origin[1], origin[2]);

	drawTetra(tetraPt);
	glPopMatrix();
	
	Sleep(30);
	glutSwapBuffers();
	glFlush();
}

void idle()
{
	if (animOption == 1)
	{
		xAngle += xStep;
		if (xAngle > 360)
		{
			xAngle = 0.0;
		}
	}
	else if (animOption == 2)
	{
		if (xPos >= 253.073)
			xPos = ref1[0], zPos = ref1[2];

		xPos += transStep;
		zPos += grad * transStep;
	}
	else if (animOption == 3)
	{
		if (xPos >= 253.073)
			xPos = ref1[0], zPos = ref1[2], scaleCur = 1;

		xPos += transStep;
		zPos += grad * transStep;
		scaleCur *= scaleFactor;
	}
	glutPostRedisplay();
}

void keyFunc(GLubyte key, GLint xMouse, GLint yMouse)
{
	switch (key)
	{
	case 's': glutIdleFunc(idle); break;
	case 't': glutIdleFunc(NULL); break;
	default: break;
	}
	glutPostRedisplay();
}

void animationSelection(GLint animationOption)
{
	switch (animationOption) {
	case 1:
		animOption = 1;
		break;
	case 2:
		xPos = ref1[0], yPos = ref1[1], zPos = ref1[2];
		animOption = 2;
		break;
	case 3:
		xPos = ref1[0], yPos = ref1[1], zPos = ref1[2];
		animOption = 3;
		break;
	default:
		break;
	}

	xAngle = yAngle = zAngle = 0.0;

	glutPostRedisplay();
}

int main(int argc, char** argv)
{
	GLint animSubMenu;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE);

	glutInitWindowPosition(50, 50);
	glutInitWindowSize(windowWidth, windowHeight);
	glutCreateWindow("3D shape");

	glutCreateMenu(animationSelection);
	glutAddMenuEntry("rotate", 1);
	glutAddMenuEntry("translate", 2);
	glutAddMenuEntry("scale", 3);
	glutAttachMenu(GLUT_LEFT_BUTTON);

	glutDisplayFunc(display);
	glutIdleFunc(idle);
	glutKeyboardFunc(keyFunc);

	init();

	glutMainLoop();
}