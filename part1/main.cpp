#include <iostream>
#define _USE_MATH_DEFINES   
#include <math.h>
#include <Windows.h>

// for using the functions in the libraries of GL, GLU and glut
#include <GL/glut.h>      

using namespace std;

GLint clippingWindowHeight = 0.0;
GLint clippingWindowWidth = 0.0;
GLint screenDisplayHeight = 0.0;
GLint screenDisplayWidth = 0.0;
GLdouble side = 36.0;

int numberOfShapes = 0;

GLdouble sx = 1;
GLdouble sy = 1;
GLdouble sz = 1;

GLint displayObject = 1;
GLint animation = 1;

GLdouble shapeVert[8][2];

int squareSize = 400;
int maxSquares = 104;
double scaleFactor = 0.95;
int refreshMills = 70;

double rotation;
int numSquares = 1;

//init function holds all initializations and related one-time parameter settings 
void init(void)
{
	// Set the background color to be white
	glClearColor(0.0, 0.0, 0.0, 1.0);  // the last value is the alpha value, 0.0 means opaque material.
									   // glColor3f(0,0,0);

									   //Set the current matrix to be the PROJECTION matrix
	glMatrixMode(GL_PROJECTION);

	// Set the current matrix to an identiy matrix whose diagonal elements are 1 and other elements are zero.
	glLoadIdentity();

	/*
	Set projection parameters.
	the clipping window is of size windowWidth x windowHeight
	*/
	//gluOrtho2D (0.0, clippingWindowWidth, 0.0, clippingWindowHeight);

	// equivalent function call to gluOtho2D
	// glOrtho(0.0, clippingWindowWidth, 0.0, clippingWindowHeight, 1.0, -1.0); // for 2D, zNear and zFar can take any vaue as long as zFar > zNear
	glOrtho(-200, 200, -200, 200, 1.0, -1.0);
	//Set the current matrix to be the MODELVIEW matrix
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

}

void calculateVerts() {
	GLdouble angleRad = M_PI * 45 / 180;

	shapeVert[0][0] = 0;
	shapeVert[0][1] = side;

	shapeVert[1][0] = 0;
	shapeVert[1][1] = 0;

	shapeVert[2][0] = side * cos(angleRad);
	shapeVert[2][1] = side * sin(angleRad);

	shapeVert[3][0] = side * cos(angleRad);
	shapeVert[3][1] = side * sin(angleRad) + side;

	shapeVert[4][0] = 0;
	shapeVert[4][1] = 2 * sin(angleRad) * side + side;

	shapeVert[5][0] = 0 - (side * cos(angleRad));
	shapeVert[5][1] = side * sin(angleRad) + side;

	shapeVert[6][0] = shapeVert[0][0];
	shapeVert[6][1] = shapeVert[0][1];

	shapeVert[7][0] = shapeVert[3][0];
	shapeVert[7][1] = shapeVert[3][1];

}

void idle() {

	switch(animation){
		case 1: 
			if (numberOfShapes >= 8) {
				numberOfShapes = 0;
			}
			else {
				numberOfShapes++;
			}
			break;
		case 2:
			numberOfShapes = 8;
			numSquares = maxSquares;
			break;
		default:
			cout << "Invalid animation option." << endl;
	}

	

	glutPostRedisplay();
}

double calculateRotation(double size, double scale)
{
	double sizeScaled = scale * size;
	double rotationD;

	double hypotLength = pow(sizeScaled / 2, 2);
	hypotLength += hypotLength;
	hypotLength = sqrt(hypotLength);
	rotationD = 45 - acos(size / 2 / hypotLength) * 180 / M_PI;

	return rotationD;
}

void Timer(int value) 
{	
	glutPostRedisplay();

	if (numSquares < maxSquares)
		glutTimerFunc(refreshMills, Timer, 0);
}

void displayFlower() {
	glPushMatrix();

	glLineWidth(5);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	for (int i = 0; i < numberOfShapes; i++)
	{
		glRotatef(45, 0, 0, -1);
		glBegin(GL_POLYGON);

		for (int j = 0; j < 8; j++)
		{
			glVertex2dv(shapeVert[j]);
		}
		glEnd();
	}
	glLineWidth(1);

	glPopMatrix();
}

void displaySquares(){
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	double newSize = squareSize;
	
	for (int i = 0; i < numSquares; i++)
	{
		
		glPushMatrix();
		glRotatef(rotation *i, 0, 0, 1);
		if(i!=0)
			glScalef(scaleFactor, scaleFactor, 1);
		glRectf(-1 * newSize / 2, newSize / 2, newSize / 2, -1 * newSize / 2);
		glPopMatrix();

		if(i!=0)
			newSize *= scaleFactor;
	}
	numSquares++;
}

void displayCallback()
{
	glClear(GL_COLOR_BUFFER_BIT);
	

	switch(displayObject){
		case 1:
			displayFlower();
			break;
		default:
			displaySquares();
			break;
	}

	// glScalef(sx, sy, sz);

	Sleep(500);
	glutSwapBuffers();
	glFlush();
};

void displaySelection(GLint animationOption){
	animation = animationOption;
}

void objectSelection(GLint objectOption){
	displayObject = objectOption;
}

int main(int argc, char** argv)
{
	calculateVerts();

	/*Initialize GLUT. It is similar to the main function, which take two arguments,
	one is the address to a string consisting of a full list of all of the command line arguments,
	and the other is the numberof arguments
	*/
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);   // Set display mode.
	glutInitWindowPosition(550, 10);   // Set top-left display-window position.

	glutInitWindowSize(800, 800);      // Set display-window width and height.
	glutCreateWindow("Group 1-He-Richardson Final Part 1"); // Create display window.

	init();                            // Execute initialization procedure.
	rotation = calculateRotation(squareSize, scaleFactor);


	GLint subMenu = glutCreateMenu(displaySelection);
	glutAddMenuEntry("Animated", 1);
	glutAddMenuEntry("Final", 2);

	glutCreateMenu(objectSelection);
	glutAddMenuEntry("Flower", 1);
	glutAddMenuEntry("Squares", 2);
	// glutAddMenuEntry("Flower", 1);

	glutAddSubMenu("Display", subMenu);
	glutAttachMenu(GLUT_LEFT_BUTTON);

	glutDisplayFunc(displayCallback);
	glutIdleFunc(idle);

	glutMainLoop();                    // Display everything and wait.

	return 0;
}