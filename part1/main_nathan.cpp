#define _USE_MATH_DEFINES

#include <glut.h>
#include <math.h> 

int squareSize = 400;
int maxSquares = 104;
double scaleFactor = 0.95;
int refreshMills = 70;

double rotation;
int numSquares = 1;

void init(void)
{
	glClearColor(0, 0, 0, 0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluOrtho2D(-200, 200, -200, 200);
}

void Timer(int value) 
{	
	glutPostRedisplay();

	if (numSquares < maxSquares)
		glutTimerFunc(refreshMills, Timer, 0);
}

double calculateRotation(double size, double scale)
{
	double sizeScaled = scale * size;
	double rotationD;

	double hypotLength = pow(sizeScaled / 2, 2);
	hypotLength += hypotLength;
	hypotLength = sqrt(hypotLength);
	rotationD = 45 - acos(size / 2 / hypotLength) * 180 / M_PI;

	return rotationD;
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);


	double newSize = squareSize;
	
	for (int i = 0; i < numSquares; i++)
	{
		
		glPushMatrix();
		glRotatef(rotation *i, 0, 0, 1);
		if(i!=0)
			glScalef(scaleFactor, scaleFactor, 1);
		glRectf(-1 * newSize / 2, newSize / 2, newSize / 2, -1 * newSize / 2);
		glPopMatrix();

		if(i!=0)
			newSize *= scaleFactor;
	}
	numSquares++;
	glFlush();
	glutSwapBuffers();
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE);
	glutInitWindowSize(400, 400);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("2D Animation");
	glutDisplayFunc(display);

	init();
	rotation = calculateRotation(squareSize, scaleFactor);

	glutTimerFunc(0, Timer, 0);

	glutMainLoop();
}